import React, { useEffect, useState, useRef, useImperativeHandle, forwardRef } from 'react';
import { View, SafeAreaView, Button, StyleSheet, FlatList, TouchableOpacity, Text, Dimensions, StatusBar, Image, Modal } from 'react-native';
import jwt from "react-native-pure-jwt"
import { RTCPeerConnection, RTCView, mediaDevices, RTCIceCandidate, RTCSessionDescription } from 'react-native-webrtc';
import { log, logError } from '../debug';
import io from 'socket.io-client';
import InCallManager from 'react-native-incall-manager';
import Mid from './Mid'
import BandwidthHandler from './BandwidthHandler';
import BandWidth from './BandWidth';
const { width, height } = Dimensions.get('screen')
const OFFER = "OFFER"
const ANSWER = "ANSWER"
const CANDIDATE = "CANDIDATE"
const LEAVE = "LEAVE"
const REJECT = "REJECT"
const DELINE = "DELINE"
var qvgaConstraints = {
    maxWidth: 320,
    maxHeight: 180,
    minFrameRate: 30,
};

var vgaConstraints = {
    maxWidth: 640,
    maxHeight: 360,
    minFrameRate: 30,
};

var hdConstraints = {
    minWidth: 1280,
    minHeight: 720,
    minFrameRate: 30,
};
function CallScreen({ isVisible, data, onAnswer, onClose, setData }, ref) {
    // const [localStream, setLocalStream] = useState();
    const [state, _setState] = useState({ id: '', isAnswer: false, makeCall: false });
    const [remoteStream, setRemoteStream] = useState();
    const [isMuted, setIsMuted] = useState(false);
    const [isSpeak, setIsSpeak] = useState(true);
    const socket = useRef()
    const localStream = useRef()
    const socketId = useRef("")
    const socketId2 = useRef("")
    const localPC = useRef()
    useImperativeHandle(ref, () => ({
        startCall: (id, isOffer) => startCall(id, isOffer)
    }), [])
    const setState = (data = { id: '', isAnswer: false }) => {
        _setState(state => {
            return { ...state, ...data }

        })
    }

    const onSend = (type, data = {}, callback) => {
        // if (socketId) {
        //     data.to = socketId
        // }
        socket.current.emit(type, data, callback)
    }
    useEffect(() => {
        const didmount = async () => {
            try {
                socket.current = await Mid.connectSocket(data.loginToken)
                socket.current.on('count-user', (data2) => {
                    setData(data2.filter(e => e != data.id))
                });
                socket.current.on(OFFER, async (data) => {
                    debugger
                    onAnswer()
                    setState({ data2: data })
                    if (!localPC.current) {
                        await initCall(localStream.current)
                    }
                    await localPC.current.setRemoteDescription(new RTCSessionDescription(data.sdp));
                    // createAnswer(data)
                })
                socket.current.on(ANSWER, async (data) => {
                    debugger
                    console.log('localPC, setRemoteDescription');
                    setState({ isAnswer: true })
                    await localPC.current.setRemoteDescription(new RTCSessionDescription(data.sdp));
                    // exchange(data)
                });
                socket.current.on(CANDIDATE, data => {
                    // exchange(data);
                    debugger
                    localPC.current.addIceCandidate(new RTCIceCandidate(data.candidate));
                });
                socket.current.on(LEAVE, socketId => {
                    closeStreams(socketId);
                });
                socket.current.on(REJECT, socketId => {
                    closeStreams(socketId);
                });
                socket.current.on(DELINE, socketId => {
                    closeStreams(socketId);
                });
                startLocalStream(true)
            } catch (error) {
                console.log('error: ', error);

            }
        }
        didmount()
        InCallManager.requestRecordPermission().then(res => {
            console.log('res: ', res);
            if (res == 'granted') {

            }
        }).catch(err => {

        })
    }, [])
    const startLocalStream = async (init) => {
        console.log(11221122)

        debugger
        try {
            // isFront will determine if the initial camera should face user or environment
            const isFront = true;
            const devices = await mediaDevices.enumerateDevices();

            const facing = isFront ? 'front' : 'environment';
            const videoSourceId = devices.find(device => device.kind === 'videoinput' && device.facing === facing);
            const facingMode = isFront ? 'user' : 'environment';
            const constraints = {
                audio: true,
                // audio: {
                //     echoCancellation: true,
                //     noiseSuppression: true,
                //     autoGainControl: true,
                //     googEchoCancellation: true,
                //     // googAutoGainControl: true,
                //     googNoiseSuppression: true,
                //     // googHighpassFilter: true,
                //     // googTypingNoiseDetection: true,
                //     googNoiseReduction: true,
                //     volume: 0.9,
                // },
                video: {
                    mandatory: hdConstraints,
                    facingMode,
                    optional: videoSourceId ? [{ sourceId: videoSourceId }] : [],
                },
            };
            const newStream = await mediaDevices.getUserMedia(constraints);
            if (init) {
                initCall(newStream)
            }
            onSend("STREAM", newStream)
            localStream.current = newStream
            // setLocalStream(newStream);
        } catch (error) {
            console.log('error: ', error);
            debugger

        }

    };
    const getStats = () => {
        const pc = localPC.current;
        if (pc.getRemoteStreams()[0] && pc.getRemoteStreams()[0].getAudioTracks()[0]) {
            const track = pc.getRemoteStreams()[0].getAudioTracks()[0];
            let callback = report => console.log('getStats report', report);

            //console.log('track', track);

            pc.getStats(track).then(callback).catch(logError);
        }
    };
    const initCall = async (newStream) => {
        debugger

        // You'll most likely need to use a STUN server at least. Look into TURN and decide if that's necessary for your project
        const configuration = {
            iceServers: [
                {
                    url: 'turn:numb.viagenie.ca',
                    credential: 'muazkh',
                    username: 'webrtc@live.com'
                },
                {
                    url: 'turn:192.158.29.39:3478?transport=udp',
                    credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
                    username: '28224511:1379330808'
                },
                {
                    url: 'turn:192.158.29.39:3478?transport=tcp',
                    credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
                    username: '28224511:1379330808'
                },
                {
                    url: 'turn:turn.bistri.com:80',
                    credential: 'homeo',
                    username: 'homeo'
                },
                {
                    url: 'turn:turn.anyfirewall.com:443?transport=tcp',
                    credential: 'webrtc',
                    username: 'webrtc'
                }
            ],
            iceTransportPolicy: 'all',
            bundlePolicy: 'max-bundle',
            rtcpMuxPolicy: 'require'
            // iceServers: [
            //     // {
            //     //   urls: 'turn:s2.xirsys.com:80?transport=tcp',
            //     //   username: '8a63bcac-e16a-11e7-a86e-a62bc0457e71',
            //     //   credential: '8a63bdd8-e16a-11e7-b7e2-48f12b7ac2d8'
            //     // },
            //     {
            //         urls: 'turn:turn.msgsafe.io:443?transport=tcp',
            //         username: 'a9a2b514',
            //         credential: '00163e7826d6'
            //     },
            //     /* Native libraries DO NOT fail over correctly.
            //     {
            //       urls: 'turn:turn.msgsafe.io:443',
            //       username: 'a9a2b514',
            //       credential: '00163e7826d6'
            //     }
            //     */
            // ]
        };
        localPC.current = new RTCPeerConnection(configuration);
        // remotePC.current = new RTCPeerConnection(configuration);

        // could also use "addEventListener" for these callbacks, but you'd need to handle removing them as well
        localPC.current.onnegotiationneeded = e => {

        }
        localPC.current.onicecandidate = e => {
            try {
                debugger
                console.log('localPC icecandidate:', e.candidate);
                if (e.candidate) {
                    onSend(CANDIDATE, { to: socketId2.current, candidate: e.candidate, type: 'local' })
                    // remotePC.current.addIceCandidate(new RTCIceCandidate(e.candidate));
                }
            } catch (err) {
                debugger
                console.error(`Error adding remotePC iceCandidate: ${err}`);
            }
        };
        localPC.current.onaddstream = e => {
            console.log('remotePC tracking with ', e);
            debugger
            if (e.stream && remoteStream !== e.stream) {
                console.log('RemotePC received the stream', e.stream);
                setRemoteStream(e.stream);
            }
        };

        localPC.current.addStream(newStream);
        /**
                * On Ice Connection State Change
                */

        localPC.current.oniceconnectionstatechange = event => {
            console.log('event: oniceconnectionstatechange', event);
            console.log('event: iceConnectionState', event.target.iceConnectionState);

            //console.log('oniceconnectionstatechange', event.target.iceConnectionState);
            if (event.target.iceConnectionState === 'completed') {
                //console.log('event.target.iceConnectionState === 'completed'');
                setTimeout(() => {
                    getStats();
                }, 1000);
            }

        };

        /**
         * On Signaling State Change
         */
        localPC.current.onsignalingstatechange = event => {
            console.log('event: ', event);
            //console.log('on signaling state change', event.target.signalingState);
        };

        /**
         * On Remove Stream
         */
        localPC.current.onremovestream = event => {
            debugger
            //console.log('on remove stream', event.stream);
        };
    };

    function setBandwidth(sdp) {
        const audioBandwidth = 50;
        const videoBandwidth = 256;
        sdp = sdp.replace(/a=mid:audio\r\n/g, 'a=mid:audio\r\nb=AS:' + audioBandwidth + '\r\n');
        sdp = sdp.replace(/a=mid:video\r\n/g, 'a=mid:video\r\nb=AS:' + videoBandwidth + '\r\n');
        return sdp;
    }
    function handle_offer_sdp(offer) {
        let sdp = offer.sdp.split('\r\n');//convert to an concatenable array
        let new_sdp = '';
        let position = null;
        sdp = sdp.slice(0, -1); //remove the last comma ','
        for (let i = 0; i < sdp.length; i++) {//look if exists already a b=AS:XXX line
            if (sdp[i].match(/b=AS:/)) {
                position = i; //mark the position
            }
        }
        if (position) {
            sdp.splice(position, 1);//remove if exists
        }
        for (let i = 0; i < sdp.length; i++) {
            if (sdp[i].match(/m=video/)) {//modify and add the new lines for video
                new_sdp += sdp[i] + '\r\n' + 'b=AS:' + '128' + '\r\n';
            }
            else {
                if (sdp[i].match(/m=audio/)) { //modify and add the new lines for audio
                    new_sdp += sdp[i] + '\r\n' + 'b=AS:' + 64 + '\r\n';
                }
                else {
                    new_sdp += sdp[i] + '\r\n';
                }
            }
        }
        return new_sdp; //return the new sdp
    }
    const startCall = async (id, isOffer) => {
        debugger
        try {
            debugger
            if (!localPC.current) {
                await initCall(localStream.current)
            }
            socketId2.current = id
            if (isOffer) {
                InCallManager.start({ media: "video" });
                setState({ makeCall: true })
                const offer = await localPC.current.createOffer();
                // offer.sdp = handle_offer_sdp(offer)
                // offer.sdp = BandwidthHandler.setOpusAttributes(offer.sdp, {
                //     'stereo': 0, // to disable stereo (to force mono audio)
                //     'sprop-stereo': 1,
                //     'maxaveragebitrate': 500 * 1024 * 8, // 500 kbits
                //     'maxplaybackrate': 500 * 1024 * 8, // 500 kbits
                //     'cbr': 0, // disable cbr
                //     'useinbandfec': 1, // use inband fec
                //     'usedtx': 1, // use dtx
                //     'maxptime': 3
                // });
                console.log('Offer from localPC, setLocalDescription');
                await localPC.current.setLocalDescription(offer);
                console.log('remotePC, setRemoteDescription');
                onSend(OFFER, { to: id, from: socketId.current, sdp: localPC.current.localDescription }, (res) => {
                    debugger
                })
            }
        } catch (err) {
            debugger
            console.log(err);
        }

    }
    const createAnswer = async () => {
        try {
            const { data2 } = state
            debugger

            InCallManager.start({ media: "video" });
            const answer = await localPC.current.createAnswer();
            console.log(`Answer from remotePC: ${answer.sdp}`);
            console.log('remotePC, setLocalDescription');
            // answer.sdp = handle_offer_sdp(answer)

            // answer.sdp = BandwidthHandler.setOpusAttributes(answer.sdp, {
            //     'stereo': 0, // to disable stereo (to force mono audio)
            //     'sprop-stereo': 1,
            //     'maxaveragebitrate': 500 * 1024 * 8, // 500 kbits
            //     'maxplaybackrate': 500 * 1024 * 8, // 500 kbits
            //     'cbr': 0, // disable cbr
            //     'useinbandfec': 1, // use inband fec
            //     'usedtx': 1, // use dtx
            //     'maxptime': 3
            // });
            await localPC.current.setLocalDescription(answer);
            setState({ isAnswer: true })
            onSend(ANSWER, { to: data2.from, sdp: localPC.current.localDescription })
        } catch (error) {

        }

    }
    const switchCamera = () => {
        debugger
        localStream.current.getVideoTracks().forEach(track => track._switchCamera());
    };

    // Mutes the local's outgoing audio
    const toggleMute = () => {
        debugger
        if (!remoteStream) return;
        localStream.current.getAudioTracks().forEach(track => {
            console.log(track.enabled ? 'muting' : 'unmuting', ' local track', track);
            track.enabled = !track.enabled;
            setIsMuted(!track.enabled);
        });
    };

    const closeStreams = () => {
        if (localPC.current) {
            debugger
            // localPC.current.removeStream(localStream);
            localPC.current.close();
            localPC.current = null
        }
        InCallManager.stop();
        setState({ isAnswer: false, makeCall: false })
        setIsSpeak(true)
        setIsMuted(false)
        // setLocalStream();
        // setRemoteStream();
        onClose()


    };
    const toggleSpeaker = () => {
        setIsSpeak(!isSpeak)
        console.log('isSpeak: ', isSpeak);
        InCallManager.setForceSpeakerphoneOn(!isSpeak);

    }
    const rejectCall = () => {
        if (state?.data2?.from) {
            let type = state.isAnswer || state.makeCall ? Mid.LEAVE : Mid.REJECT
            console.log('type: ', type);
            onSend(LEAVE, { to: state.data2.from, type })
        }
        closeStreams()
    }
    return (
        <Modal
            animated={true}
            animationType="slide"
            transparent={false}
            visible={isVisible}>
            <View style={styles.container}>
                <StatusBar translucent={true} backgroundColor={'transparent'} />
                <View style={[styles.rtcview, { height, ...StyleSheet.absoluteFillObject, zIndex: 0 }]}>
                    {remoteStream && <RTCView style={styles.rtc} zOrder={-1} objectFit="cover" streamURL={remoteStream.toURL()} />}
                </View>

                {localPC.current ?
                    <BandWidth localPc={localPC.current} track />
                    : null
                }
                <View style={[styles.rtcview, {
                    height: '30%',
                    width: '40%',
                    borderRadius: 5,
                    alignSelf: 'flex-end',
                    marginRight: 5,
                    zIndex: 10
                }]}>
                    {localStream.current && <RTCView style={[styles.rtc]} zOrder={1} streamURL={localStream.current.toURL()} />}
                    <TouchableOpacity onPress={switchCamera} style={styles.buttonSwitch}>
                        <Image source={require('../res/videoCall/camera_switch.png')} style={styles.iconSwitch} />
                    </TouchableOpacity>
                </View>
                <View style={{
                    flex: 1,
                    justifyContent: 'flex-end'
                }}>
                    {localStream.current && (
                        <View style={styles.toggleButtons}>
                            <TouchableOpacity onPress={toggleMute} style={{ padding: 10 }}>
                                {isMuted ?
                                    <Image source={require('../res/videoCall/mute_selected.png')} style={styles.icon} />
                                    :
                                    <Image source={require('../res/videoCall/mute.png')} style={styles.icon} />
                                }
                            </TouchableOpacity>
                            <TouchableOpacity onPress={toggleSpeaker} style={{ padding: 10 }}>
                                {isSpeak ?
                                    <Image source={require('../res/videoCall/speaker_selected.png')} style={styles.icon} />
                                    :
                                    <Image source={require('../res/videoCall/speaker.png')} style={styles.icon} />
                                }
                            </TouchableOpacity>

                        </View>
                    )}
                    <View style={styles.toggleButtons}>
                        {!state.isAnswer && !state.makeCall ?
                            <TouchableOpacity onPress={createAnswer} style={{ padding: 10 }}>
                                <Image source={require('../res/videoCall/accept_call.png')} style={styles.icon} />
                            </TouchableOpacity>
                            : null
                        }
                        <TouchableOpacity onPress={() => rejectCall(Mid.LEAVE)} style={{ padding: 10 }}>
                            <Image source={require('../res/videoCall/end_call.png')} style={styles.icon} />
                        </TouchableOpacity>

                    </View>
                </View>

            </View>

        </Modal>
    );
}

const styles = StyleSheet.create({
    icon: {
        height: 60,
        width: 60
    },
    buttonSwitch: {
        padding: 10,
        position: 'absolute',
        bottom: 10,
        marginBottom: 10,
        alignSelf: 'center',
    },
    iconSwitch: {
        height: 40,
        width: 40,

    },
    container: {
        backgroundColor: '#313131',
        // justifyContent: 'space-between',
        // alignItems: 'center',
        flex: 1
    },
    text: {
        fontSize: 30,
    },
    rtcview: {
        // backgroundColor: 'black',
    },
    rtc: {
        width: '100%',
        height: '100%',
    },
    toggleButtons: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
});


export default forwardRef(CallScreen);