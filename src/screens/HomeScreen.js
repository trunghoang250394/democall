import React, { useEffect, useState, useRef } from 'react'
import { View, Text, FlatList, TouchableOpacity, StyleSheet } from 'react-native'
import userProvider from '../utils/user-provider'
import io from 'socket.io-client';
import CallScreen from './CallScreen';

const HomeScreen = ({ navigation }) => {
    let data = navigation.getParam('data')
    const [state, _setState] = useState({
        page: 0,
        size: 50,
        data: [],
        dataConnected: [],
        isVisible: false
    })
    const videoCall = useRef()
    const socket = useRef()
    const setState = (data = {}) => {
        _setState(state => ({
            ...state, ...data
        }))
    }
    useEffect(() => {
        const getData = () => {
            const { page, size } = state
            console.log('getData')

            userProvider.getListDoctor(page, size).then(res => {
                console.log('res: ', res);
                if (res && res.length > 0) {
                    // setState({ data: res })
                } else {
                }
            }).catch(err => {

            })
        }
        // connectSocket()
        getData()
    }, [])
    const connectSocket = async () => {
        return new Promise(async (resolve, reject) => {
            try {
                let data = navigation.getParam('data')
                console.log('data: ', data);
                let token = data.loginToken
                if (token) {
                    console.log('token: ', token);
                    // const url = 'http://192.168.1.5:4443';
                    const url = 'http://10.0.50.244:4443';
                    // const url = 'https://react-native-webrtc.herokuapp.com';
                    // const url = 'https://node-js-webbrtc-server.herokuapp.com';
                    socket.current = io.connect(url, {
                        transports: ['websocket'], query: {
                            token: token
                        }
                    })
                    socket.current.connect()
                    socket.current.on('count-user', (data2) => {
                        console.log('data: ', data2);
                        setState({
                            data: data2.filter(e => e != data.id)
                        })
                    });
                    resolve()

                }
            } catch (error) {
                console.log('error: ', error);
                reject()
            }
        })


    }
    const onCallScreen = (item) => () => {
        navigation.navigate('videoCall', {
            item
        })
    }
    const renderItem = ({ item, index }) => {
        let isOnline = state.dataConnected.find(e => e == item.userId)
        console.log('isOnline: ', isOnline);
        return (
            <TouchableOpacity
                onPress={onCallScreen(item)}
                style={styles.containerItem}>
                {isOnline ?
                    <View style={{
                        backgroundColor: 'blue',
                        height: 8,
                        width: 8,
                        borderRadius: 4
                    }} /> : null}
                <Text>{item.userId}</Text>
                <Text>{item.telephone}</Text>
                <Text>{item.name}</Text>
            </TouchableOpacity>
        )
    }
    const keyExtractor = (item, index) => index.toString()
    const startCall = (item) => {
        setState({ isVisible: true })
        videoCall.current.startCall(item, true)
        // navigation.navigate('videoCall', {
        //     item,
        //     isOffer: true,
        // })
    }
    const onAnswer = () => {
        setState({ isVisible: true })
    }
    const onClose = () => {
        setState({ isVisible: false })
    }
    const setData = (data) => {
        setState({ data })
    }
    return (
        <View style={{
            flex: 1
        }}>
            {/* <FlatList
                renderItem={renderItem}
                keyExtractor={keyExtractor}
                data={state.data}
            /> */}
            <FlatList
                data={state.data}
                horizontal={true}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => {
                    return (
                        <TouchableOpacity
                            onPress={() => startCall(item, true)}
                            style={{
                                backgroundColor: 'green',
                                padding: 10,
                                borderRadius: 5,
                                margin: 5,
                                height: 50
                            }}>
                            <Text>{item}</Text>
                        </TouchableOpacity>
                    )
                }}
            />
            <CallScreen
                isVisible={state.isVisible}
                data={data}
                ref={videoCall}
                onAnswer={onAnswer}
                onClose={onClose}
                setData={setData}
            />
        </View>
    )
}

export default HomeScreen


const styles = StyleSheet.create({
    containerItem: {
        paddingVertical: 15,
        width: '100%',
        borderBottomColor: '#00000050',
        borderBottomWidth: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingHorizontal: 20,
    },
})