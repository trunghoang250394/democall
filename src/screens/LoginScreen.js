import React, { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, TextInput } from 'react-native'
import userProvider from '../utils/user-provider'
import AsyncStorage from '@react-native-community/async-storage'
import NavigationService from '../navigators/NavigationService'
const LoginScreen = () => {
    const [state, _setState] = useState({ username: '', password: '' })
    const setState = (data = {}) => {
        _setState(state => ({
            ...state, ...data
        }))
    }
    const onChangeText = (state) => value => {
        setState({ [state]: value })
    }
    useEffect(() => {
        const getName = async () => {
            try {
                let username = await AsyncStorage.getItem("user")
                let password = await AsyncStorage.getItem("pass")
                setState({ username, password })
                
            } catch (error) {
                
            }
        }
        getName()
    }, [])
    const onLogin = async () => {
        try {
            let res = await userProvider.login(state.username, state.password)
            console.log('res: ', res);
            if (res?.code == 0) {
                AsyncStorage.setItem("user", state.username)
                AsyncStorage.setItem("pass", state.password)
                NavigationService.navigate('home', { data: res.data.user })
            }
        } catch (error) {
            console.log('error: ', error);

        }
    }
    return (
        <View style={styles.container}>

            <TextInput style={styles.input}
                placeholder="TK..."
                value={state.username}
                keyboardType="number-pad"
                onChangeText={onChangeText("username")}
            />
            <TextInput style={styles.input}
                value={state.password}
                keyboardType="number-pad"
                placeholder="MK..."
                onChangeText={onChangeText("password")}
            />
            <TouchableOpacity
                onPress={onLogin}
                style={styles.buttonLogin}>
                <Text>LOGIN</Text>
            </TouchableOpacity>
        </View>
    )
}

export default LoginScreen


const styles = StyleSheet.create({
    buttonLogin: {
        backgroundColor: 'green',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 5
    },
    input: {
        borderRadius: 5,
        borderColor: '#00000060',
        borderWidth: 1,
        height: 40,
        width: '80%',
        margin: 10
    },
    container: {
        flex: 1,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
})