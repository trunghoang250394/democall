import { RTCPeerConnection, RTCView, mediaDevices, RTCIceCandidate, RTCSessionDescription, } from 'react-native-webrtc';
import io from 'socket.io-client';
const OFFER = "OFFER"
const ANSWER = "ANSWER"
const CANDIDATE = "CANDIDATE"
const LEAVE = "LEAVE"
const REJECT = "REJECT"
const DELINE = "DELINE"
const connectSocket = async (token) => {
    let socket = null
    return new Promise(async (resolve, reject) => {
        try {
            if (token) {
                console.log('token: ', token);
                const url = 'http://192.168.1.5:4443';
                // const url = 'http://192.168.43.31:4443';
                // const url = 'http://10.0.50.244:4443';
                // const url = 'https://react-native-webrtc.herokuapp.com';
                // const url = 'https://node-js-webbrtc-server.herokuapp.com';
                socket = io.connect(url, {
                    transports: ['websocket'], query: {
                        token: token
                    }
                })
                socket.connect()
                resolve(socket)

            }
        } catch (error) {
            console.log('error: ', error);
            reject()
        }
    })


}

export default {
    connectSocket,
    OFFER,
    ANSWER,
    CANDIDATE,
    LEAVE,
    DELINE,
    REJECT,

}