var selector = pc.getRemoteStreams()[0].getAudioTracks()[0];
var rttMeasures = [];
 
// ... wait a bit
var aBit = 1000; // in milliseconds
setTimeout(function () {
    pc.getStats(selector, function (report) {
    for (var i in report) {
        // the below parsing may not work correctly in any browser
        // as they have differing implementations.
        var now = report[i];
        if (now.type == "outboundrtp") {
            // this is an example based on mid-2015,
            // the object returned by stats API
            // use googRTT for chrome/opera or mozRTT for firefox.
            // the objects are expected to change, the standard is documented at:
            // http://www.w3.org/TR/webrtc-stats/
            rttMeasures.append(now.roundTripTime);
            var avgRTT = average(rttMeasures);
            // this is a very simple emodel and does not take
            // packetization time, or inter-frame delay metrics into account.
            // You may calculate the e-value at each sample
            // or at the end of the call.
            var emodel = 0;
            if (avgRTT/2 >= 500)
                emodel = 1;
            else if (avgRTT/2 >= 400)
                emodel = 2;
            else if (avgRTT/2 >= 300)
                emodel = 3;
            else if (avgRTT/2 >= 200)
                emodel = 4;
            else if (avgRTT/2 < 200)
                emodel = 5;
            //console.log ("e-model: "+str(emodel));
          }
        }
     }, logError);
}, aBit);
 
function average (values) {
    var sumValues = values.reduce(function(sum, value){
        return sum + value;
    }, 0);
    return (sumValues / values.length);
    } function logError(error) {
        log(error.name + ": " + error.message);
    }