import client from "./client-utils";
import { Platform } from "react-native";
import md5 from 'md5'
const os = Platform.select({
  ios: 2,
  android: 2
});
export default {
  deviceId: "",
  deviceToken: "",

  login(username, password) {
    return new Promise((resolve, reject) => {
      var body = {
        emailOrPhone: username,
        password: md5(password),
        device: { os: os, deviceId: this.deviceId, token: this.deviceToken }
      };
      client.requestApi("put", "isofhcare/user/login", body, (s, e) => {
        if (s) resolve(s);
        else reject(e);
      });
    });
  },
  getListDoctor(page, size) {
    return new Promise((resolve, reject) => {

      client.requestApi(
        "get",
        client.serviceSchedule +
        'isofhcare/catalog/v1/doctor-availables?page=' + Number(page) + '&size=' + Number(size),
        {},
        (s, e) => {
          if (s) resolve(s);
          reject(e);
        }
      );
    });
  },
};
