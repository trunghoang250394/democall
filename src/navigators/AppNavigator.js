import React from 'react';

import { createDrawerNavigator, DrawerItems, createBottomTabNavigator, TabBarBottom, createStackNavigator, createAppContainer } from 'react-navigation';
import LoginScreen from '../screens/LoginScreen';
import CallScreen from '../screens/CallScreen';
import HomeScreen from '../screens/HomeScreen';

const handleCustomTransition = ({ scenes }) => {
  const prevScene = scenes[scenes.length - 2];
  const nextScene = scenes[scenes.length - 1];
  if (prevScene
    && prevScene.route.routeName === 'home'
    && nextScene.route.routeName === 'listSpecialist') {
    return fromBottom();
  }
  return fromRight();
}


const RootNavigator = createStackNavigator(
  {
    login: { screen: LoginScreen },
    videoCall: { screen: CallScreen },
    home: { screen: HomeScreen },
  },
  {
    headerMode: "none",
    header: null,
    gesturesEnabled: false,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    },
    // transitionConfig: (nav) => handleCustomTransition(nav)
  }
);
export default createAppContainer(RootNavigator)
