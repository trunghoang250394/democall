import React, { useEffect, useState, useRef } from 'react';
import { View, SafeAreaView, StyleSheet, } from 'react-native';
import AppContainer from './src/navigators/AppNavigator'
import NavigationService from './src/navigators/NavigationService';
export default function App() {
    return (
        <SafeAreaView style={{ flex: 1 }} >
            <AppContainer ref={navigatorRef => {
                NavigationService.setTopLevelNavigator(navigatorRef);
            }}
            />
        </SafeAreaView>
    );
}
