import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TouchableHighlight,
    View,
    TextInput,
    Platform,
    value,
    FlatList,
    TouchableOpacity,
    Image,
    Dimensions
} from 'react-native';
import ListView from "deprecated-react-native-listview";
import io from 'socket.io-client';

// const socket = io.connect('http://10.0.50.127:4443', { transports: ['websocket'] });
const socket = io.connect('https://react-native-webrtc.herokuapp.com', { transports: ['websocket'] });
// const socket = io.connect(apis.BASE_SOCKET, { transports: ['websocket'] });

import {
    RTCPeerConnection,
    RTCIceCandidate,
    RTCSessionDescription,
    RTCView,
    MediaStream,
    MediaStreamTrack,
    mediaDevices
} from 'react-native-webrtc';
const { height, width } = Dimensions.get('window');
// const configuration = {
//     iceServers: [
//         { urls: ['stun:stun.l.google.com:19302', 'stun:stun1.l.google.com:19302'] },
//         // {
//         //   'urls': 'turn:45.252.248.114:3478?transport=udp',
//         //   'credential': 'B0h3m14nrh4ps0dy',
//         //   'username': 'vskin8899'
//         // },
//         // {
//         //   urls: 'turn:45.252.248.114:3478?transport=tcp',
//         //   credential: 'B0h3m14nrh4ps0dy',
//         //   username: 'vskin8899'
//         // }
//     ]
// };
const configuration = { "iceServers": [{ "url": "stun:stun.l.google.com:19302" }] };
export default class App extends Component {
    constructor(props) {
        super(props);
        this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => true });
        this.state = {
            info: 'Initializing',
            status: 'init',
            roomID: '',
            isFront: true,
            selfViewSrc: null,
            remoteList: {},
            textRoomConnected: false,
            textRoomData: [],
            textRoomValue: '',
        }
        this.localStream
        this.pcPeers = {}
        socket.on('exchange', (data) => {
            this.exchange(data);
        });
        socket.on('leave', (socketId) => {
            this.leave(socketId);
        });

        socket.on('connect', (data) => {
            
            this.getLocalStream(true, (stream) => {
                
                this.localStream = stream;
                this.setState({ selfViewSrc: stream.toURL() });
                this.setState({ status: 'ready', info: 'Please enter or create room ID' });
            });
        });
    }
    getLocalStream = (isFront, callback) => {
        let videoSourceId;
        // on android, you don't have to specify sourceId manually, just use facingMode
        // uncomment it if you want to specify
        if (Platform.OS === 'ios') {
            mediaDevices.enumerateDevices().then(sourceInfos => {
                for (const i = 0; i < sourceInfos.length; i++) {
                    const sourceInfo = sourceInfos[i];
                    if (sourceInfo.kind == "videoinput" && sourceInfo.facing == (isFront ? "front" : "back")) {

                        videoSourceId = sourceInfo.id;
                    }
                }
            });
        }
        mediaDevices.getUserMedia({

            audio: true,
            video: {
                mandatory: {
                    minWidth: 640, // Provide your own width, height and frame rate here
                    minHeight: 360,
                    minFrameRate: 30,
                },
                facingMode: (isFront ? "user" : "environment"),
                optional: (videoSourceId ? [{ sourceId: videoSourceId }] : []),

            }

        }).then((stream) => {
            
            callback(stream);
        }, this.logError)

    }

    join = (roomID) => {
        socket.emit('join', roomID, (socketIds) => {
            
            for (const i in socketIds) {
                const socketId = socketIds[i];
                
                this.createPC(socketId, true);
            }
        });
    }

    createPC = (socketId, isOffer) => {
        const pc = new RTCPeerConnection(configuration);
        
        this.pcPeers[socketId] = pc;

        pc.onicecandidate = (event) => {
            if (event.candidate) {
                socket.emit('exchange', { 'to': socketId, 'candidate': event.candidate });
            }
        };

        const createOffer = () => {
            pc.createOffer().then((desc) => {
                
                pc.setLocalDescription(desc).then(() => {
                    socket.emit('exchange', { 'to': socketId, 'sdp': pc.localDescription });
                }, this.logError);
            }, this.logError);
        }

        pc.onnegotiationneeded = () => {
            
            if (isOffer) {
                createOffer()
            }
        }

        pc.oniceconnectionstatechange = (event) => {
            
            if (event.target.iceConnectionState === 'completed') {
                setTimeout(() => {
                    this.getStats()
                }, 1000)
            }
            if (event.target.iceConnectionState === 'connected') {
                createDataChannel();
            }
        };
        pc.onsignalingstatechange = (event) => {
            
        };

        pc.onaddstream = (event) => {
            
            this.setState({ info: 'One peer join!' });

            const remoteList = this.state.remoteList;
            debugger;
            remoteList[socketId] = event.stream.toURL();
            this.setState({ remoteList: remoteList });
        };
        pc.onremovestream = (event) => {
            
        };

        
        pc.addStream(this.localStream);
        const createDataChannel = () => {
            if (pc.textDataChannel) {
                return;
            }
            const dataChannel = pc.createDataChannel("text");

            dataChannel.onerror = (error) => {
                
            };

            dataChannel.onmessage = (event) => {
                
                this.receiveTextData({ user: socketId, message: event.data });
            };

            dataChannel.onopen = () => {
                
                this.setState({ textRoomConnected: true });
            };

            dataChannel.onclose = () => {
                
            };

            pc.textDataChannel = dataChannel;
        }
        return pc;
    }

    exchange = (data) => {
        const fromId = data.from;
        let pc;
        if (fromId in this.pcPeers) {
            pc = this.pcPeers[fromId];
        } else {
            pc = this.createPC(fromId, false);
        }

        if (data.sdp) {
            
            pc.setRemoteDescription(new RTCSessionDescription(data.sdp)).then(() => {
                if (pc.remoteDescription.type == "offer")
                    pc.createAnswer().then((desc) => {
                        
                        pc.setLocalDescription(desc).then(() => {
                            
                            socket.emit('exchange', { 'to':{socketId: fromId} , 'sdp': pc.localDescription });
                        }).catch(e=>{
                            debugger;
                            console.log(e);
                        });
                    }).catch(e=>{
                        debugger;
                        console.log(e);
                    });
            }).catch(e=>{
                debugger;
                console.log(e);
            });
        } else {
            
            pc.addIceCandidate(new RTCIceCandidate(data.candidate));
        }
    }

    leave = (socketId) => {
                const pc = this.pcPeers[socketId];
        
        pc.close();
        delete this.pcPeers[socketId];
        const remoteList = this.state.remoteList;
        delete remoteList[socketId]
        this.setState({ remoteList: remoteList });
        this.setState({ info: 'One peer leave!' });
    }

    logError = (error) => {
        
    }

    mapHash = (hash, func) => {
        const array = [];
        for (const key in hash) {
            
            const obj = hash[key];
            array.push(func(obj, key));
        }
        return array;
    }

    getStats = () => {
        const pc = this.pcPeers[Object.keys(this.pcPeers)[0]];
        if (pc.getRemoteStreams()[0] && pc.getRemoteStreams()[0].getAudioTracks()[0]) {
            const track = pc.getRemoteStreams()[0].getAudioTracks()[0];
            
            pc.getStats(track).then((report) => {
                
            }, this.logError);
        }
    }
    _press = (event)=> {
        if(this.refs)
        this.refs.roomID.blur(event)
        this.setState({ status: 'connect', info: 'Connecting' }, () => {
            this.join(event)
        })

    }
    _switchVideoType=()=> {

        const isFront = !this.state.isFront;

        this.setState({ isFront });
        // 
        this.getLocalStream(isFront, (stream) => {
            if (this.localStream) {
                for (const id in this.pcPeers) {
                    const pc = this.pcPeers[id];
                    pc && pc.removeStream(this.localStream);
                }
                this.localStream.release();
            }
            this.localStream = stream;
            this.setState({ selfViewSrc: stream.toURL() });

            for (const id in this.pcPeers) {
                const pc = this.pcPeers[id];
                pc && pc.addStream(this.localStream);
            }
        });
    }
    receiveTextData = (data) => {
        const textRoomData = this.state.textRoomData.slice();
        textRoomData.push(data);
        this.setState({ textRoomData, textRoomValue: '' });
    }
    _textRoomPress() {
        if (!this.state.textRoomValue) {
            return
        }
        const textRoomData = this.state.textRoomData.slice();
        textRoomData.push({ user: 'Me', message: this.state.textRoomValue });
        for (const key in this.pcPeers) {
            const pc = this.pcPeers[key];
            pc.textDataChannel.send(this.state.textRoomValue);
        }
        this.setState({ textRoomData, textRoomValue: '' });
    }
    _renderTextRoom() {
        return (
            <View style={styles.listViewContainer}>
                <ListView
                    dataSource={this.ds.cloneWithRows(this.state.textRoomData)}
                    renderRow={rowData => <Text>{`${rowData.user}: ${rowData.message}`}</Text>}
                />
                <TextInput
                    style={styles.sendMessageText}
                    onChangeText={value => this.setState({ textRoomValue: value })}
                    value={this.state.textRoomValue}
                />
                <TouchableHighlight
                    onPress={this._textRoomPress.bind(this, this.state.textRoomValue)}>
                    <Text>Send</Text>
                </TouchableHighlight>
            </View>
        );
    }
    closeVideoCall = () => {
        const pc = this.pcPeers[socketId];
        if (pc) {

            pc.close();
            delete this.pcPeers[socketId];
            const remoteList = this.state.remoteList;
            delete remoteList[socketId]
            this.setState({ remoteList: remoteList });
            this.setState({ info: 'One peer leave!' });
        }
    }
    render() {
        let data = [this.state.selfViewSrc, this.state.selfViewSrc, this.state.selfViewSrc, this.state.selfViewSrc]
        return (
            <View style={styles.container}>
        <Text style={styles.welcome}>
          {this.state.info}
        </Text>
        {this.state.textRoomConnected && this._renderTextRoom()}
        <View style={{ flexDirection: 'row' }}>
          <Text>
            {this.state.isFront ? "Use front camera" : "Use back camera"}
          </Text>
          <TouchableHighlight
            style={{ borderWidth: 1, borderColor: 'black' }}
            onPress={this._switchVideoType}>
            <Text>Switch camera</Text>
          </TouchableHighlight>
        </View>
        {this.state.status == 'ready' ?
          (<View>
            <TextInput
              ref='roomID'
              autoCorrect={false}
              style={{ width: 200, height: 40, borderColor: 'gray', borderWidth: 1 }}
              onChangeText={(text) => this.setState({ roomID: text })}
              value={this.state.roomID}
            />
            <TouchableHighlight
              onPress={this._press}>
              <Text>Enter room</Text>
            </TouchableHighlight>
          </View>) : null
        }
        <RTCView streamURL={this.state.selfViewSrc} style={styles.selfView} />
        {
          this.mapHash(this.state.remoteList, function (remote, index) {
            return <RTCView key={index} streamURL={remote} style={styles.remoteView} />
          })
        }
      </View>
        );
    }
};
const styles = StyleSheet.create({
    selfView: {
      width: 200,
      height: 150,
    },
    remoteView: {
      width: 200,
      height: 150,
    },
    container: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    listViewContainer: {
      height: 150,
    },
  });