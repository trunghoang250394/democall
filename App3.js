import React from 'react';
import SocketIO from 'socket.io-client'
import WebSocketClient from 'reconnecting-websocket'
import UUID from 'uuid'

import {
    RTCPeerConnection,
    RTCIceCandidate,
    RTCSessionDescription,
    RTCView,
    MediaStream,
    MediaStreamTrack,
    mediaDevices
} from 'react-native-webrtc';
import { StyleSheet, Text, View, TouchableOpacity, Dimensions, SafeAreaView } from 'react-native';

const dimensions = Dimensions.get('window')

const HOST = 'wss://192.168.1.5:8443'
const isFront = true // Use Front camera?
const DEFAULT_ICE = {
    // we need to fork react-native-webrtc for relay-only to work.
    //  iceTransportPolicy: "relay",
    iceServers: [
        // {
        //   urls: 'turn:s2.xirsys.com:80?transport=tcp',
        //   username: '8a63bcac-e16a-11e7-a86e-a62bc0457e71',
        //   credential: '8a63bdd8-e16a-11e7-b7e2-48f12b7ac2d8'
        // },
        {
            urls: 'turn:turn.msgsafe.io:443?transport=tcp',
            username: 'a9a2b514',
            credential: '00163e7826d6'
        },
        /* Native libraries DO NOT fail over correctly.
        {
          urls: 'turn:turn.msgsafe.io:443',
          username: 'a9a2b514',
          credential: '00163e7826d6'
        }
        */
    ]
}

export default class App extends React.Component {

    constructor(props) {
        super(props)
        this.handleConnect = this.handleConnect.bind(this)
        this.on_ICE_Connection_State_Change = this.on_ICE_Connection_State_Change.bind(this)
        this.on_Add_Stream = this.on_Add_Stream.bind(this)
        this.on_ICE_Candiate = this.on_ICE_Candiate.bind(this)
        this.sendMessage = this.sendMessage.bind(this)
        this.on_Offer_Received = this.on_Offer_Received.bind(this)
        this.on_Answer_Received = this.on_Answer_Received.bind(this)
        this.setupWebRTC = this.setupWebRTC.bind(this)
        this.handleAnswer = this.handleAnswer.bind(this)
        this.on_Remote_ICE_Candidate = this.on_Remote_ICE_Candidate.bind(this)

        this.state = {
            connected: false,
            ice_connection_state: '',
            pendingCandidates: []
        }
    }

    render() {
        console.log('this.state.localStreamURL: ', this.state.localStreamURL);
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.container}>
                    <View style={styles.video}>
                        <View style={styles.callerVideo}>
                            <View style={styles.videoWidget}>
                                {this.state.localStreamURL &&
                                    <RTCView streamURL={this.state.localStreamURL} style={styles.rtcView} />
                                }
                            </View>
                        </View>
                        <View style={styles.calleeVideo}>
                            <View style={styles.videoWidget}>
                                {this.state.remoteStreamURL &&
                                    <RTCView streamURL={this.state.remoteStreamURL} style={styles.rtcView} />
                                }
                            </View>
                        </View>
                    </View>
                    <View style={this.state.connected ? styles.onlineCircle : styles.offlineCircle} />
                    <View style={styles.bottomView}>
                        <TouchableOpacity onPress={this.handleConnect} disabled={this.state.offer_received}>
                            <Text style={styles.connect}>
                                Connect
            </Text>
                        </TouchableOpacity>
                        { // Offer received and offer not answered
                            (this.state.offer_received && !this.state.offer_answered) &&
                            <TouchableOpacity onPress={this.handleAnswer}>
                                <Text style={styles.connect}>
                                    Answer
              </Text>
                            </TouchableOpacity>
                        }
                    </View>
                </View>
            </SafeAreaView>
        );
    }

    async setupWebRTC() {
        const self = this
        const peer = new RTCPeerConnection(DEFAULT_ICE)
        peer.oniceconnectionstatechange = this.on_ICE_Connection_State_Change
        peer.onaddstream = this.on_Add_Stream
        peer.onicecandidate = this.on_ICE_Candiate

        console.info('localStream:', this.localStream)
        peer.addStream(this.localStream)
        this.peer = peer
    }

    async handleConnect(e) {
        await this.setupWebRTC()
        const { peer } = this

        try {
            // Create Offer
            const offer = await peer.createOffer()
            console.info('Offer Created:', offer)
            self.offer = offer
            console.info('offer:', offer)

            await peer.setLocalDescription(offer)
            console.info('localDescription set!')

            // TODO: should send localDescription or offer
            // For now send localDescription


        } catch (e) {
            console.log('Failed to setup local offer')
            console.log(e.message)
            return
        }
    }

    on_ICE_Connection_State_Change(e) {
        console.info('ICE Connection State Changed:', e.target.iceConnectionState)
        this.setState({
            ice_connection_state: e.target.iceConnectionState
        })

        switch (e.target.iceConnectionState) {
            case 'closed':
            case 'disconnected':
            case 'failed':
                if (this.peer) {
                    this.peer.close()
                    this.setState({
                        remoteStreamURL: null
                    })
                    this.remoteStream = null
                }
                break
        }
    }

    on_ICE_Candiate(e) {
        const { candidate } = e
        console.info('ICE Candidate Found:', candidate)

        if (candidate) {
            let pendingRemoteIceCandidates = this.state.pendingCandidates
            if (Array.isArray(pendingRemoteIceCandidates)) {
                this.setState({
                    pendingCandidates: [...pendingRemoteIceCandidates, candidate]
                })
            } else {
                this.setState({
                    pendingCandidates: [candidate]
                })
            }
        } else { // Candidate gathering complete
            if (this.state.pendingCandidates.length > 1) {
                this.sendMessage({
                    type: this.state.offer_received ? 'answer' : 'offer',
                    payload: {
                        description: this.peer.localDescription,
                        candidates: this.state.pendingCandidates
                    }
                })
            } else {
                console.log('Failed to send an offer/answer: No candidates')
                debugger
            }
        }
    }

    async on_Remote_ICE_Candidate(data) {
        if (data.payload) {
            if (this.peer) {
                await this.peer.addIceCandidate(new RTCIceCandidate(data.payload))
            } else {
                console.log('Peer is not ready')
            }
        } else {
            console.info('Remote ICE Candidates Gathered!')
        }
    }

    on_Add_Stream(e) {
        console.info('Remote Stream Added:', e.stream)
        this.setState({
            remoteStreamURL: e.stream.toURL()
        })
        this.remoteStream = e.stream
    }

    on_Offer_Received(data) {
        debugger
        this.setState({
            offer_received: true,
            offer_answered: false,
            offer: data
        })
    }

    async on_Answer_Received(data) {
        const { payload } = data
        await this.peer.setRemoteDescription(new WebRTCLib.RTCSessionDescription(payload.description))
        payload.candidates.forEach(c => this.peer.addIceCandidate(new RTCIceCandidate(c)))
        this.setState({
            answer_recevied: true
        })
    }

    async handleAnswer() {
        const { payload } = this.state.offer
        await this.setupWebRTC()

        const { peer } = this

        await peer.setRemoteDescription(new WebRTCLib.RTCSessionDescription(payload.description))

        if (Array.isArray(payload.candidates)) {
            payload.candidates.forEach((c) => peer.addIceCandidate(new RTCIceCandidate(c)))
        }
        const answer = await peer.createAnswer()
        await peer.setLocalDescription(answer)
        this.setState({
            offer_answered: true
        })
    }

    sendMessage(msgObj) {
        console.log('msgObj: ', msgObj);
        const { ws } = this
        console.log('ws.readyState: ', ws.readyState);
        if (ws && ws.readyState === 1) {
            ws.send(JSON.stringify(msgObj))
        } else {
            const e = {
                code: 'websocket_error',
                message: 'WebSocket state:' + ws.readyState
            }
            // throw e
        }
    }

    componentWillUnmount() {
        if (this.peer) {
            this.peer.close()
        }
    }
    gotMessageFromServer = (message) => {
        console.log("Got message", message.data);
        var data = JSON.parse(message.data);

        switch (data.type) {
            case "login":
                handleLogin(data.success, data.allUsers);
                break;
            //when somebody wants to call us 
            case "offer":
                console.log('inside offer')
                this.handleOffer(data.offer, data.name);
                break;
            case "answer":
                console.log('inside answer')
                this.handleAnswer(data.answer);
                break;
            //when a remote peer sends an ice candidate to us 
            case "candidate":
                console.log('inside handle candidate')
                this.handleCandidate(data.candidate);
                break;
            case "leave":
                this.handleLeave();
                break;
            default:
                break;
        }
    }
    send(msg) {
        //attach the other peer username to our messages 
        // if (connectedUser) {
        //     msg.name = connectedUser;
        // }
        console.log('msg before sending to server', msg)
        this.ws.send(JSON.stringify(msg));
    };
    componentDidMount() {

        // Setup Socket
        const ws = new WebSocketClient(HOST);
        const self = this
        self.ws = ws

        ws.onopen = () => {
            console.info('Socket Connected!')
            self.setState({
                connected: true
            })
        };

        ws.onmessage = this.gotMessageFromServer

        ws.onerror = e => {
            // an error occurred
            console.info(e.message, 'err');
            self.setState({
                connected: false
            })
        };

        ws.onclose = e => {
            // connection closed
            console.log(e.code, e.reason);
            self.setState({
                connected: false
            })
        };

        // Setup Camera & Audio
        mediaDevices.enumerateDevices()
            .then(sourceInfos => {
                let videoSourceId;
                for (const i = 0; i < sourceInfos.length; i++) {
                    const sourceInfo = sourceInfos[i];
                    if (sourceInfo.kind == "videoinput" && sourceInfo.facing == (isFront ? "front" : "back")) {

                        videoSourceId = sourceInfo.id;
                    }
                }
                return mediaDevices.getUserMedia({
                    audio: true,
                    video: {
                        mandatory: {
                            minWidth: 500, // Provide your own width, height and frame rate here
                            minHeight: 300,
                            minFrameRate: 30
                        },
                        facingMode: (isFront ? "user" : "environment"),
                        optional: (videoSourceId ? [{ sourceId: videoSourceId }] : [])
                    }
                });
            })
            .then(stream => {
                console.log('stream: ', stream.toURL());
                this.setState({
                    localStreamURL: stream.toURL()
                })
                this.localStream = stream
            })
            .catch(e => {
                console.log('Failed to setup stream:', e.message)
            })
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    bottomView: {
        height: 20,
        flex: 1,
        bottom: 80,
        position: 'absolute',
        alignItems: 'center'
    },
    connect: {
        fontSize: 30
    },
    video: {
        flex: 1,
        flexDirection: 'row',
        position: 'relative',
        backgroundColor: '#eee',
        alignSelf: 'stretch'
    },
    onlineCircle: {
        width: 20,
        height: 20,
        borderRadius: 10,
        backgroundColor: '#1e1',
        position: 'absolute',
        top: 10,
        left: 10
    },
    offlineCircle: {
        width: 20,
        height: 20,
        borderRadius: 10,
        backgroundColor: '#333'
    },
    callerVideo: {
        flex: 0.5,
        backgroundColor: '#faa',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    calleeVideo: {
        flex: 0.5,
        backgroundColor: '#aaf',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    videoWidget: {
        position: 'relative',
        flex: 0.5,
        backgroundColor: '#fff',
        width: dimensions.width / 2,
        borderWidth: 1,
        borderColor: '#eee'
    },
    rtcView: {
        flex: 1,
        width: dimensions.width / 2,
        backgroundColor: '#f00',
        position: 'relative'
    }
});