import React, { Component } from 'react';
import { Text, TouchableOpacity, View, YellowBox, SafeAreaView, FlatList, StyleSheet } from 'react-native';

import {
    RTCPeerConnection,
    RTCIceCandidate,
    RTCSessionDescription,
    RTCView,
    MediaStream,
    MediaStreamTrack,
    mediaDevices
} from 'react-native-webrtc';
import io from 'socket.io-client';
import { button, container, rtcView, text } from './src2/styles';
import { log, logError } from './src2/debug';
import jwt from "react-native-pure-jwt"

const OFFER = "OFFER"
const ANSWER = "ANSWER"
const CANDIDATE = "CANDIDATE"
const LOGIN = "LOGIN"
YellowBox.ignoreWarnings(['Setting a timer', 'Unrecognized WebSocket connection', 'ListView is deprecated and will be removed']);

/* ==============================
 Global variables
 ================================ */
// const url = 'https://react-native-webrtc.herokuapp.com';
// const url = 'http://10.0.50.127:4443';

const configuration = { iceServers: [{ urls: 'stun:stun.l.google.com:19302' }] };

let pcPeers = {};
let appClass;
let localStream;

/* ==============================
 Class
 ================================ */
class App extends Component {
    state = {
        info: 'Initializing',
        status: 'init',
        roomID: 'abc',
        isFront: true,
        streamURL: null,
        remoteList: null,
        data: [],
        id: ''
    };

    constructor(props) {
        super(props);
        this.socket = null
        /**
        * Create the Peer Connection
        */
        this.yourConn = new RTCPeerConnection(configuration);
        this.socketId = null
    }
    connectSocket = async () => {
        return new Promise(async (resolve, reject) => {
            try {
                let id = Math.floor(Math.random() * 100).toString()
                console.log('id: ', id);
                this.setState({
                    id
                })
                let token = await jwt.sign({
                    _id: id,
                    exp: new Date().getTime() + 24 * 1000 * 60, // expiration date, required, in ms, absolute to 1/1/1970
                    additional: "payload"
                }, // body
                    "com.isofh.isofhcare", // secret
                    {
                        alg: "HS256",
                    })
                if (token) {
                    const url = 'http://192.168.1.5:4443';
                    // const url = 'http://10.0.50.244:4443';
                    // const url = 'https://react-native-webrtc.herokuapp.com';
                    // const url = 'https://node-js-webbrtc-server.herokuapp.com';
                    this.socket = io.connect(url, {
                        transports: ['websocket'], query: {
                            token: token
                        }
                    })
                    resolve()

                }
            } catch (error) {
                reject()
            }
        })


    }
    /* ==============================
     Functions
     ================================ */
    getLocalStream = () => {
        let isFront = true;
        let videoSourceId;
        // on android, you don't have to specify sourceId manually, just use facingMode
        // uncomment it if you want to specify
        if (Platform.OS === 'ios') {
            mediaDevices.enumerateDevices().then(sourceInfos => {
                for (let i = 0; i < sourceInfos.length; i++) {
                    const sourceInfo = sourceInfos[i];
                    if (sourceInfo.kind == "videoinput" && sourceInfo.facing == (isFront ? "front" : "back")) {

                        videoSourceId = sourceInfo.id;
                    }
                }
            });
        }
        let constrains = {
            audio: false,
            video: {
                mandatory: {
                    minWidth: 640,
                    minHeight: 360,
                    minFrameRate: 30,
                },
                facingMode: isFront ? 'user' : 'environment',
            },
        };
        let getStream = stream => {
            console.log('stream: ', stream);
            this.localStream = stream;

            this.setState({
                streamURL: stream.toURL(),
                status: 'ready',
                info: 'Welcome to WebRTC demo',
            });
        };

        mediaDevices.getUserMedia(constrains).then(getStream).catch(logError);
    };
    createPC = (socketId, isOffer) => {
        this.socketId = socketId
        debugger
        /**
         * On Negotiation Needed
         */
        this.yourConn.onnegotiationneeded = () => {
            //console.log('onnegotiationneeded');
            if (isOffer) {
                let callback2 = () => {
                    //console.log('setLocalDescription', this.yourConn.localDescription);
                    this.onSend(OFFER, { sdp: this.yourConn.localDescription })
                    // this.socket.emit(OFFER, { to: this.socketId, sdp: this.yourConn.localDescription });
                };
                let callback = desc => {

                    log('The SDP offer', desc.sdp);

                    this.yourConn.setLocalDescription(desc).then(callback2).catch(logError);
                };

                this.yourConn.createOffer().then(callback).catch(logError);
            }
        };

        /**
         * (Deprecated)
         */
        this.yourConn.addStream(this.localStream);

        /**
         * On Add Stream (Deprecated)
         */
        this.yourConn.onaddstream = event => {
            debugger
            //console.log('onaddstream', event.stream);
            const remoteList = event.stream.toURL();
            this.setState({
                info: 'One this.yourConn join!',
                remoteList: remoteList,
            });
        };

        /**
         * On Ice Candidate
         */
        this.yourConn.onicecandidate = event => {
            //console.log('onicecandidate', event.candidate);
            if (event.candidate) {
                debugger;
                this.onSend(CANDIDATE, {  candidate: event.candidate })
                // this.socket.emit(CANDIDATE, { to: this.socketId, candidate: event.candidate });
            }
        };

        /**
         * On Ice Connection State Change
         */
        this.yourConn.oniceconnectionstatechange = event => {
            //console.log('oniceconnectionstatechange', event.target.iceConnectionState);
            if (event.target.iceConnectionState === 'completed') {
                //console.log('event.target.iceConnectionState === 'completed'');
                setTimeout(() => {
                    this.getStats();
                }, 1000);
            }
            if (event.target.iceConnectionState === 'connected') {
                //console.log('event.target.iceConnectionState === 'connected'');
            }
        };

        /**
         * On Signaling State Change
         */
        this.yourConn.onsignalingstatechange = event => {
            //console.log('on signaling state change', event.target.signalingState);
        };

        /**
         * On Remove Stream
         */
        this.yourConn.onremovestream = event => {
            //console.log('on remove stream', event.stream);
        };

    };

    exchange = data => {
        let fromId = data.from;


        debugger
        if (data.sdp) {
            log('Exchange', data);
            //console.log('exchange sdp', data);
            let sdp = new RTCSessionDescription(data.sdp);

            let callback3 = () => {
                debugger
                this.socketId = fromId
                this.onSend(ANSWER, { sdp: this.yourConn.localDescription })
            }
            let callback2 = desc => this.yourConn.setLocalDescription(desc).then(callback3).catch(logError);
            let callback = () => data.sdp.type === 'offer' ? this.yourConn.createAnswer().then(callback2).catch(logError) : null;
            this.yourConn.setRemoteDescription(sdp).then(callback).catch(logError);
        } else {
            this.yourConn.addIceCandidate(new RTCIceCandidate(data.candidate));
        }
    };

    onSend = (type, data = {}) => {
        if (this.socketId) {
            data.to = this.socketId
        }
        this.socket.emit(type, data)
    }
    leave = socketId => {
        //console.log('leave', socketId);

        // const peer = pcPeers[socketId];

        this.yourConn.close();

        this.localStream.release()
        this.setState({
            info: 'One peer left!',
            remoteList: null,
        });
    };

    mapHash = (hash, func) => {
        //console.log(hash);
        const array = [];
        for (const key in hash) {
            if (hash.hasOwnProperty(key)) {
                const obj = hash[key];
                array.push(func(obj, key));
            }
        }
        return array;
    };

    getStats = () => {
        const pc = this.yourConn;
        if (pc.getRemoteStreams()[0] && pc.getRemoteStreams()[0].getAudioTracks()[0]) {
            const track = pc.getRemoteStreams()[0].getAudioTracks()[0];
            let callback = report => console.log('getStats report', report);

            //console.log('track', track);

            pc.getStats(track).then(callback).catch(logError);
        }
    };


    async  componentDidMount() {
        try {
            await this.connectSocket()
            this.socket.on('count-user', (data) => {
                this.setState({
                    data: data.filter(e => e != this.state.id)
                })
            });
            this.getLocalStream();
            this.socket.on(OFFER, (data) => {
                debugger
                this.socketId = data.to
                this.exchange(data)
            })
            this.socket.on(ANSWER, (data) => {
                debugger
                this.exchange(data)
            });
            this.socket.on(CANDIDATE, data => {
                this.exchange(data);
            });
            this.socket.on('leave', socketId => {
                this.leave(socketId);
            });

        } catch (error) {
            console.log('error: ', error);

        }


    }

    switchCamera = () => {
        this.localStream.getVideoTracks().forEach(track => {
            track._switchCamera();
        });
    };

    onPress = () => {
        this.setState({
            status: 'connect',
            info: 'Connecting',
        });

        this.join(this.state.roomID);
    };

    button = (func, text) => (
        <TouchableOpacity style={button.container} onPress={func}>
            <Text style={button.style}>{text}</Text>
        </TouchableOpacity>
    );

    render() {
        const { status, info, streamURL, remoteList } = this.state;
        console.log('streamURL: ', streamURL);

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={container.style}>
                    <Text style={text.style}>{info}</Text>
                    <View>
                        <FlatList
                            data={this.state.data}
                            horizontal={true}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item, index }) => {
                                return (
                                    <TouchableOpacity
                                        onPress={() => this.createPC(item, true)}
                                        style={{
                                            backgroundColor: 'green',
                                            padding: 10,
                                            borderRadius: 5,
                                            margin: 5
                                        }}>
                                        <Text>{item}</Text>
                                    </TouchableOpacity>
                                )
                            }}
                        />
                    </View>
                    {status === 'ready' ? this.button(this.onPress, 'Enter room') : null}
                    {this.button(this.switchCamera, 'Change Camera')}
                    {streamURL ?
                        <RTCView streamURL={streamURL} style={rtcView.style} />
                        : null
                    }
                    {remoteList ?
                        <RTCView streamURL={remoteList} style={rtcView.style} />
                        : null}
                    <View>
                        {this.button(this.onAnswer, "Answer")}
                        {this.button(this.onAnswer, "Deline")}
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

/* ==============================
 Export
 ================================ */
export default App;
